# Create small random file.
file="randfile"
head -c 1024 test_files/dogs.jpg > $file

# Compute number of blocks.
filesize=$(wc -c $file | awk '{{ print $1 }}')
blocksize=128
blocks=$(($filesize / $blocksize))

for i in $(seq 0 $blocks); do
    tail -c +$(($i * $blocksize + 1)) $file | head -c $blocksize; sleep 0.75;
done | ./split - a > output

diff output <(./rsplit $file a)
rc=$?

rm -f output $file
if [[ $rc -ne 0 ]]; then
    exit 1
fi

exit 0
