# Tests for split on a large text file (frankenstein by Shelly)
text="test_files/frankenstein.txt"
diff <(./split $text a) <(./rsplit $text a) > /dev/null
