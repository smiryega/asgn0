# Test if split on two individual files gives the same result as split
# on two files at the same time

diff \
    <(cat <(./split test_files/dogs.jpg a) <(./split test_files/frankenstein.txt a)) \
    <(./split test_files/dogs.jpg test_files/frankenstein.txt a) > /dev/null
