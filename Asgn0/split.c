#include <err.h>
#include <fcntl.h> //open()
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> //read()

#define EOS '\0'

void split(char *infileName, char delim);

void split(char *infileName, char delim) {

  char buf[100];
  int fp;
  if (infileName[0] == '-') {
    fp = STDIN_FILENO;
  } else if ((fp = open(infileName, O_RDONLY, 0)) == -1) {
    warn(" %s: No such file or directory\n", infileName);
    exit(1);
  }
  while (read(fp, buf, 1) != 0) {

    if (buf[0] == delim) {
      if (write(STDOUT_FILENO, "\n", 1) == -1) {
        warn("%s", infileName);
      }
    } else {
      if (write(STDOUT_FILENO, buf, 1) != 1) {
        warn("%s", infileName);
      }
    }
  }

  close(fp);
}

int main(int argc, char *argv[]) {
  char delim;
  int N;

  N = argc;

  if (argc < 3) {
    printf("split: not enough arguments\n");
    printf("usage: ./split <files> <delimiter>\n");
    exit(1);
  }
  if (strlen(argv[N - 1]) != 1) {
    printf("split: only single-character delimiters allowed\n");
    printf("usage: ./split <files> <delimiter>\n");
    exit(1);
  }
  delim = argv[N - 1][0];

  for (int i = 1; i < N - 1; i++) {
    split(argv[i], delim);
  }

  return 0;
}
