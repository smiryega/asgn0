# Tests for split on a small text file.
# Specifically words to Louis Armstrong's What a Wonderful World.
text="test_files/wonderful.txt"
diff <(./split $text a) <(./rsplit $text a) > /dev/null
